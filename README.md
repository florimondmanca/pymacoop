# pymacoop

Python-based version of [Permacoop](https://github.com/fairnesscoop/permacoop). Meant as a lab project for a 90%-of-usecases web dev stack and digital sobriety applied to Python web development.

## Requirements

* Python 3.11+
* PostgreSQL 15+

## Quickstart

```bash
# Install dependencies
make install

# Run database migrations and add initial data
make initdb

# Run the server
make start

# See help
make help
```

The development server will be available at http://localhost:8000. Connect using the following username/password dev credentials: jane/orderlytimber.

## Credits

* [Django](https://www.djangoproject.com)
* [Modest JS Works](https://modestjs.works/)
* [Hypermedia Systems](https://hypermedia.systems/)
* [_hyperscript](https://hyperscript.org)

## License

MIT
