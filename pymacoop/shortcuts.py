from pathlib import Path
from typing import Any

from django.http import HttpRequest, HttpResponse
from django.shortcuts import render


def render_hx(
    request: HttpRequest, name: str, *args: Any, **kwargs: Any
) -> HttpResponse:
    if request.META.get("HTTP_HX_REQUEST") != "true":
        name = str((p := Path(name)).with_stem(f"{p.stem}_full"))

    return render(request, name, *args, **kwargs)
