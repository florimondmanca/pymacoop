from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path("admin/", admin.site.urls),
    path("__debug__/", include("debug_toolbar.urls")),
    path("accounts/", include("pymacoop.apps.accounts.urls", namespace="accounts")),
    path("", include("pymacoop.apps.home.urls")),
    path(
        "faircalendar/",
        include("pymacoop.apps.faircalendar.urls", namespace="faircalendar"),
    ),
    path("crm/", include("pymacoop.apps.crm.urls", namespace="crm")),
]
