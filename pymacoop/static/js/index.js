// import "./lib/turbo.js";
import "./lib/stimulus.js";
import "./controllers/index.js";

document.documentElement.addEventListener("turbo:load", () => {
    if (typeof window.djdt !== "undefined") {
        djdt.init();
    }
})
