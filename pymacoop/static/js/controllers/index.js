import { Application, Controller } from "../lib/stimulus.js";

const Stimulus = window.Stimulus = Application.start();
Stimulus.debug = true;

Stimulus.register("modal", class extends Controller {
    static values = {
        key: String,
    };
    static targets = ["dialog"];
    static outlets = ["trigger"];

    connect() {
        this.dialogTarget.addEventListener("close", () => {
            if (this.dialogTarget.returnValue === this.keyValue) {
                this.dispatch('submit');
            }
        });
    }

    triggerOutletConnected(_outlet, el) {
        el.addEventListener('click', (event) => {
            event.preventDefault();
            this.open();
        });
    }

    open() {
        this.dialogTarget.showModal();
    }
});

Stimulus.register("trigger", class extends Controller { });

Stimulus.register("form-submit", class extends Controller {
    static targets = ['form'];

    submitForm() {
        this.formTarget.requestSubmit();
    }
});

Stimulus.register("fullcalendar", class extends Controller {
    static values = {
        eventsJson: String,
        addUrl: String,
    };

    connect() {
        const calendar = new window.FullCalendar.Calendar(this.element, {
            initialView: 'dayGridMonth',
            locale: 'fr',
            nowIndicator: true,
            selectable: true,
            weekends: false,
            height: 700,
            dayHeaderFormat: { weekday: 'long' },
            events: JSON.parse(this.eventsJsonValue),
            select: (info) => {
                const year = info.start.getFullYear();
                const month = info.start.getMonth() + 1;
                const day = info.start.getDate();
                window.location = this.addUrlValue.replace('/0/0/0', `/${year}/${month}/${day}`);
            }
        });

        calendar.render();
    }
})
