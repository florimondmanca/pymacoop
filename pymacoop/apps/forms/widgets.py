from django import forms

from dj_fluent import format_value as t


class Input(forms.TextInput):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.attrs.setdefault("class", "")
        self.attrs["class"] += (
            "block w-full mt-1 text-sm dark:border-gray-600 dark:bg-gray-700 "
            "focus:border-purple-400 focus:outline-none focus:shadow-outline-purple "
            "dark:text-gray-300 dark:focus:shadow-outline-gray form-input"
        )


class PasswordInput(forms.PasswordInput, Input):
    pass


class BooleanSelect(forms.Select):
    def __init__(self, *args, **kwargs):
        kwargs["choices"] = [
            (True, t("common-yes")),
            (False, t("common-no")),
        ]
        super().__init__(*args, **kwargs)
