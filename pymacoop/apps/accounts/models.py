from django.contrib.auth.models import AbstractUser
from django.db import models

from dj_fluent import format_value as t


class User(AbstractUser):
    def __str__(self):
        if self.first_name and self.last_name:
            return f"{self.first_name} {self.last_name}"

        if self.first_name:
            return self.first_name

        return self.username


class WorkerManager(models.Manager):
    def with_user(self):
        return self.select_related("user")


class Worker(models.Model):
    objects = WorkerManager()

    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name=t("accounts-workers-user-name"),
    )
    joining_date = models.DateField(
        verbose_name=t("accounts-workers-joining_date-name"),
    )
    leaving_date = models.DateField(
        blank=True,
        null=True,
        verbose_name=t("accounts-workers-leaving_date-name"),
    )
    annual_salary = models.IntegerField(
        default=0,
        verbose_name=t("accounts-workers-annual_salary-name"),
    )
    transport_fee = models.IntegerField(
        default=0,
        verbose_name=t("accounts-workers-transport_fee-name"),
    )
    sustainable_mobility_fee = models.IntegerField(
        default=0,
        verbose_name=t("accounts-workers-sustainable_mobility_fee-name"),
    )
    executive_position = models.BooleanField(
        default=True,
        verbose_name=t("accounts-workers-executive_position-name"),
    )
    health_insurance = models.BooleanField(
        default=True,
        verbose_name=t("accounts-workers-health_insurance-name"),
    )
    contract = models.CharField(
        max_length=3,
        choices=[
            ("cdi", t("accounts-workers-contract-cdi")),
            ("cdd", t("accounts-workers-contract-cdd")),
        ],
        default="cdi",
        verbose_name=t("accounts-workers-contract-name"),
    )
    working_time = models.CharField(
        max_length=9,
        choices=[
            ("full_time", t("accounts-workers-working_time-full_time")),
            ("part_time", t("accounts-workers-working_time-part_time")),
        ],
        default="full_time",
        verbose_name=t("accounts-workers-working_time-name"),
    )

    class Meta:
        verbose_name = t("accounts-workers-verbose_name")

    def __str__(self):
        return str(self.user)
