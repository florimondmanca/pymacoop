import django_tables2 as tables
from django.urls import reverse

from pymacoop.apps.tables.columns import ActionsColumn

from ..tables import DefaultTable
from .models import Worker


class WorkersTable(DefaultTable):
    user = tables.Column(verbose_name=Worker.user.field.verbose_name)
    actions = ActionsColumn(
        urls={
            "edit": lambda obj: reverse("accounts:workers_edit", kwargs={"pk": obj.pk}),
        },
        model=Worker,
    )

    class Meta(DefaultTable.Meta):
        pass
