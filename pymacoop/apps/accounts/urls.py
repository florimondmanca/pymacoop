from django.contrib.auth import views as auth_views
from django.urls import path

from . import views

app_name = "accounts"

urlpatterns = [
    path(
        "login/",
        views.LoginView.as_view(),
        name="login",
    ),
    path(
        "logout/",
        auth_views.LogoutView.as_view(),
        name="logout",
    ),
    path(
        "password-change/",
        views.PasswordChangeView.as_view(),
        name="password_change",
    ),
    path(
        "password-reset/",
        auth_views.PasswordResetView.as_view(),
        name="password_reset",
    ),
    path(
        "edit/",
        views.edit_account,
        name="edit",
    ),
    path("workers", views.list_workers, name="workers_list"),
    path("workers/add", views.add_worker, name="workers_add"),
    path("workers/<int:pk>/edit", views.edit_worker, name="workers_edit"),
]
