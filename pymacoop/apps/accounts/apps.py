from django.apps import AppConfig

from dj_fluent import format_value as t


class AccountsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "pymacoop.apps.accounts"
    label = "accounts"
    verbose_name = t("accounts-app")
