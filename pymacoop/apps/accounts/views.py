from django.contrib.auth import views as auth_views
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404, redirect, render
from django.urls.base import reverse, reverse_lazy
from django.views.decorators.http import require_http_methods
from django.views.generic.edit import UpdateView
from djaq import DjaqQuery as DQ

from dj_fluent import format_value as t

from ...stubs import AuthenticatedHttpRequest
from ..tables.utils import paginate
from .forms import LoginForm, UserChangeForm, UserCreationForm, WorkerForm
from .models import User, Worker
from .tables import WorkersTable


class LoginView(auth_views.LoginView):
    template_name = "accounts/login.html"
    form_class = LoginForm


def edit_account(request):
    if request.method == "POST":
        form = UserChangeForm(request.POST, instance=request.user)

        if form.is_valid():
            form.save()
            return redirect("home")

    return render(
        request,
        "accounts/edit.html",
        context={
            "form": UserChangeForm(instance=request.user),
            "breadcrumb_items": [
                {"title": t("accounts-edit-title")},
            ],
        },
    )


class AccountEditView(LoginRequiredMixin, UpdateView):
    template_name = "accounts/edit.html"
    form_class = UserChangeForm
    queryset = User.objects.all()
    success_url = reverse_lazy("home")

    request: AuthenticatedHttpRequest

    def get_object(self, queryset=None) -> User:
        return self.request.user

    def get_context_data(self, **kwargs) -> dict:
        title = t("accounts-edit-title")

        return {
            **super().get_context_data(**kwargs),
            "translations": {"title": title},
            "breadcrumb_items": [
                {"title": title},
            ],
        }


class PasswordChangeView(auth_views.PasswordChangeView):
    template_name = "accounts/password_change.html"
    success_url = reverse_lazy("accounts:edit")

    def get_context_data(self, **kwargs) -> dict:
        title = t("accounts-password-change")

        return {
            **super().get_context_data(**kwargs),
            "translations": {
                "title": title,
            },
            "breadcrumb_items": [
                {"title": t("accounts-title"), "path": reverse("accounts:edit")},
                {"title": title},
            ],
        }


def list_workers(request):
    workers = list(
        DQ(
            "Worker",
            """
            id as pk,
            concat(user.first_name, ' ', user.last_name) as user,
            """,
        ).objs()
    )

    table = WorkersTable(workers)
    table = paginate(table, request)

    title = t("accounts-workers-title")

    return render(
        request,
        "accounts/workers/list.html",
        {
            "title": title,
            "table": table,
            "breadcrumb_items": [
                {"title": title},
            ],
        },
    )


@require_http_methods(["GET", "POST"])
def add_worker(request):
    if request.method == "POST":
        user_form = UserCreationForm(request.POST)
        worker_form = WorkerForm(request.POST)
        if user_form.is_valid() and worker_form.is_valid():
            user = user_form.save()
            worker_form.instance.user = user
            worker_form.save()
            return redirect("accounts:workers_list")
    else:
        user_form = UserCreationForm()
        worker_form = WorkerForm()

    title = t("accounts-workers-add-title")

    return render(
        request,
        "accounts/workers/add.html",
        {
            "title": title,
            "user_form": user_form,
            "worker_form": worker_form,
            "breadcrumb_items": [
                {"title": t("accounts-title")},
                {
                    "title": t("accounts-workers-title"),
                    "path": reverse("accounts:workers_list"),
                },
                {"title": title},
            ],
        },
    )


@require_http_methods(["GET", "POST"])
def edit_worker(request, pk):
    worker = get_object_or_404(Worker.objects.with_user(), pk=pk)

    if request.method == "POST":
        form = WorkerForm(request.POST, instance=worker)
        if form.is_valid():
            form.save()
            return redirect("accounts:workers_list")
    else:
        form = WorkerForm(instance=worker)

    title = t("accounts-workers-edit-title", {"worker": worker})

    return render(
        request,
        "accounts/workers/edit.html",
        {
            "title": title,
            "form": form,
            "breadcrumb_items": [
                {
                    "title": t("accounts-workers-title"),
                    "path": reverse("accounts:workers_list"),
                },
                {"title": title},
            ],
        },
    )
