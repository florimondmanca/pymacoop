from django import forms
from django.contrib.auth.forms import AuthenticationForm, UsernameField
from django.contrib.auth.forms import UserCreationForm as BaseUserCreationForm
from django.urls.base import reverse_lazy

from dj_fluent import format_value as t

from ..forms.base import BaseForm
from ..forms.widgets import BooleanSelect
from .models import User, Worker


class LoginForm(BaseForm, AuthenticationForm):
    username = UsernameField(
        label=t("accounts-login-form-username"),
    )
    password = forms.CharField(
        label=t("accounts-login-form-password"),
        strip=False,
        widget=forms.PasswordInput,
    )


class UserCreationForm(BaseUserCreationForm):
    class Meta:
        model = User
        fields = (
            "username",
            "email",
            "first_name",
            "last_name",
            "password1",
            "password2",
        )


class ObfuscatedPasswordWidget(forms.Widget):
    template_name = "accounts/widgets/obfuscated_password.html"
    read_only = True


class ObfuscatedPasswordField(forms.Field):
    widget = ObfuscatedPasswordWidget


class UserChangeForm(forms.ModelForm):
    password = ObfuscatedPasswordField(
        label=t("accounts-login-form-password"),
        help_text=t("accounts-login-form-password-change"),
        required=False,
        disabled=True,
    )
    password_change_url = reverse_lazy("accounts:password_change")

    instance: User

    class Meta:
        model = User
        fields = (
            "username",
            "email",
            "first_name",
            "last_name",
            "password",
        )
        field_classes = {"username": UsernameField}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        password = self.fields["password"]
        start = f'<a class="text-blue-500" href="{self.password_change_url}">'
        end = "</a>"
        password.help_text = password.help_text.format(start=start, end=end)


class WorkerForm(forms.ModelForm):
    class Meta:
        model = Worker
        fields = (
            "joining_date",
            "leaving_date",
            "annual_salary",
            "transport_fee",
            "sustainable_mobility_fee",
            "health_insurance",
            "executive_position",
            "contract",
            "working_time",
        )
        widgets = {
            "joining_date": forms.DateInput(attrs={"type": "date"}),
            "leaving_date": forms.DateInput(attrs={"type": "date"}),
            "health_insurance": BooleanSelect(),
            "executive_position": BooleanSelect(),
        }
