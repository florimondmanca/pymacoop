from django.db import models

from dj_fluent import format_value as t


class Event(models.Model):
    type = models.CharField(
        max_length=24,
        choices=[
            ("mission", t("faircalendar-event-type-mission")),
            ("support", t("faircalendar-event-type-support")),
            ("dojo", t("faircalendar-event-type-dojo")),
            ("formation_conference", t("faircalendar-event-type-formation_conference")),
            ("other", t("faircalendar-event-type-other")),
        ],
        verbose_name=t("faircalendar-event-type-name"),
    )
    date = models.DateField(
        verbose_name=t("faircalendar-event-date-name"),
    )
    project = models.ForeignKey(
        "crm.Project",
        on_delete=models.CASCADE,
        verbose_name=t("faircalendar-event-project-name"),
    )
    user = models.ForeignKey(
        "accounts.User",
        on_delete=models.CASCADE,
        verbose_name=t("faircalendar-event-user-name"),
    )
    comment = models.CharField(
        max_length=256, blank=True, verbose_name=t("faircalendar-event-comment-name")
    )

    class Meta:
        verbose_name = t("faircalendar-event-verbose_name")

    def __str__(self) -> str:
        return self.date.isoformat()
