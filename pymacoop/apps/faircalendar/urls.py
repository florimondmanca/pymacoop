from django.urls import path

from . import views

app_name = "faircalendar"

urlpatterns = [
    path("", views.index, name="index"),
    path(
        "events/add/<int:year>/<int:month>/<int:day>",
        views.add_event,
        name="events_add",
    ),
    path("events/<int:pk>/edit", views.edit_event, name="events_edit"),
    path("events/<int:pk>/delete", views.delete_event, name="events_delete"),
]
