from django.apps import AppConfig

from dj_fluent import format_value as t


class FaircalendarConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "pymacoop.apps.faircalendar"
    label = "faircalendar"
    verbose_name = t("faircalendar-title")
