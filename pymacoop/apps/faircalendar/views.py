import datetime as dt
import json

from django.core.serializers.json import DjangoJSONEncoder
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse
from django.utils import formats
from django.views.decorators.http import require_GET, require_http_methods, require_POST
from djaq import DjaqQuery as DQ

from dj_fluent import format_value as t

from .forms import EventForm
from .models import Event


@require_GET
def index(request):
    events = list(
        DQ(
            "Event",
            """
            id as pk,
            type,
            date,
            project.name
            """,
        )
        .where("user.id == {user_id}")
        .context({"user_id": request.user.pk})
        .objs()
    )

    fullcalendar_events = [
        {
            # See: https://fullcalendar.io/docs/event-object
            "type": event.type,
            "start": event.date,
            "end": event.date,
            "title": (
                event.project_name
                if event.type == "mission"
                else f"faircalendar-event-type-{event.type}"
            ),
            "url": reverse("faircalendar:events_edit", kwargs={"pk": event.pk}),
            "textColor": f"var(--event-{event.type}-text)",
            "backgroundColor": f"var(--event-{event.type}-background)",
            "borderColor": f"var(--event-{event.type}-border)",
        }
        for event in events
    ]

    fullcalendar_events_json = json.dumps(fullcalendar_events, cls=DjangoJSONEncoder)

    return render(
        request,
        "faircalendar/index.html",
        {"fullcalendar_events_json": fullcalendar_events_json},
    )


@require_http_methods(["GET", "POST"])
def add_event(request, year, month, day):
    if request.method == "POST":
        form = EventForm(request.POST)

        if form.is_valid():
            form.save()
            return redirect("faircalendar:index")
    else:
        form = EventForm(
            initial={
                "date": dt.date(year, month, day),
                "user": request.user,
            }
        )

    title = t(
        "faircalendar-event-add-title",
        {"date": formats.localize(form["date"].value())},
    )

    return render(
        request,
        "faircalendar/events/add.html",
        {
            "title": title,
            "form": form,
            "breadcrumb_items": [
                {
                    "title": t("faircalendar-title"),
                    "path": reverse("faircalendar:index"),
                },
                {"title": title},
            ],
        },
    )


@require_http_methods(["GET", "POST"])
def edit_event(request, pk):
    event = get_object_or_404(Event.objects.select_related("user"), pk=pk)

    if request.method == "POST":
        form = EventForm(request.POST, instance=event)

        if form.is_valid():
            form.save()
            return redirect("faircalendar:index")
    else:
        form = EventForm(instance=event)

    return render(
        request,
        "faircalendar/events/edit.html",
        {"form": form},
    )


@require_POST
def delete_event(request, pk):
    event = get_object_or_404(Event.objects.only("id"), pk=pk)
    event.delete()
    return redirect("faircalendar:index")
