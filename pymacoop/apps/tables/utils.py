from typing import TypeVar

import django_tables2 as tables
from django.conf import settings

T = TypeVar("T", bound=tables.Table)


def paginate(table: T, request) -> T:
    per_page = getattr(settings, "TABLES_ITEMS_PER_PAGE", 25)

    config = tables.RequestConfig(request, paginate={"per_page": per_page})
    config.configure(table)

    return table
