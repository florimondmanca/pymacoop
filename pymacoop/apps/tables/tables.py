import django_tables2 as tables


class DefaultTable(tables.Table):
    class Meta:
        template_name = "tables/default.html"
