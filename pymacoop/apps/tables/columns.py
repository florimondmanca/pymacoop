from typing import Callable

import django_tables2 as tables

from dj_fluent import format_value as t


class ActionsColumn(tables.TemplateColumn):
    def __init__(self, *, urls: dict[str, Callable], **kwargs) -> None:
        self.urls = urls

        if "objname" not in kwargs:
            objname = kwargs.pop("model")._meta.object_name
        else:
            objname = kwargs.pop("objname")

        super().__init__(
            verbose_name=kwargs.pop("verbose_name", t("common-actions")),
            template_name="tables/columns/actions.html",
            extra_context={
                "delete_message": lambda: t(
                    "common-form-confirm-delete", {"obj": objname}
                ),
            },
            orderable=False,
            **kwargs,
        )

    def render(self, column: tables.TemplateColumn, record, **kwargs) -> str:
        column.extra_context["form_actions"] = {
            action: url(record) for action, url in self.urls.items()
        }

        return super().render(column=column, record=record, **kwargs)
