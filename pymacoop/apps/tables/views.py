from typing import Type

import django_tables2 as tables
from django import forms
from django.db import models, transaction
from django.http import HttpResponse
from django.urls import reverse
from django.views.generic import CreateView, ListView, UpdateView
from django.views.generic.base import ContextMixin
from django.views.generic.edit import BaseDeleteView

from ..tables import paginate


class TableUrlsMixin:
    def get_index_url(self, model: Type[models.Model]) -> str:
        return reverse(f"{model._meta.app_label}:index")

    def get_add_url(self, model: Type[models.Model]) -> str:
        return reverse(f"{model._meta.app_label}:add")

    def get_edit_url(self, obj: models.Model) -> str:
        return reverse(f"{obj.__class__._meta.app_label}:edit", kwargs={"pk": obj.pk})

    def get_delete_url(self, obj: models.Model) -> str:
        return reverse(f"{obj.__class__._meta.app_label}:delete", kwargs={"pk": obj.pk})


class BreadcrumbMixin(ContextMixin):
    def get_breadcrumbs(self) -> list:
        return []

    def get_context_data(self, **kwargs) -> dict:
        context = super().get_context_data(**kwargs)
        context["breadcrumb_items"] = self.get_breadcrumbs()
        return context


class TableListView(TableUrlsMixin, BreadcrumbMixin, ListView):
    model: Type[models.Model]
    table_class: Type[tables.Table]
    title: str
    template_name = "tables/views/index.html"

    def get_context_data(self, **kwargs) -> dict:
        qs = self.get_queryset()
        table = paginate(self.table_class(qs), self.request)

        return dict(
            **super().get_context_data(**kwargs),
            table=table,
            title=self.title,
            add_url=self.get_add_url(self.model),
        )


class TableAddView(TableUrlsMixin, BreadcrumbMixin, CreateView):
    model: Type[models.Model]
    form_class: Type[forms.BaseForm]
    template_name = "tables/views/add.html"
    title: str

    def get_success_url(self) -> str:
        return self.get_index_url(self.model)

    def get_context_data(self, **kwargs) -> dict:
        return dict(
            **super().get_context_data(**kwargs),
            title=self.title,
            form_action=self.get_add_url(self.model),
        )


class TableEditView(TableUrlsMixin, BreadcrumbMixin, UpdateView):
    model: Type[models.Model]
    form_class: Type[forms.BaseForm]
    title_template: str
    template_name = "tables/views/edit.html"

    def get_title(self) -> str:
        obj = self.object  # type: ignore
        return self.title_template.format(obj=obj)

    def get_success_url(self) -> str:
        return self.get_index_url(self.model)

    def get_context_data(self, **kwargs) -> dict:
        obj = self.object  # type: ignore
        return dict(
            **super().get_context_data(**kwargs),
            title=self.get_title(),
            form_action=self.get_edit_url(obj),
        )


class TableDeleteView(TableUrlsMixin, BaseDeleteView):
    model: Type[models.Model]
    table_class: Type[tables.Table]

    @transaction.atomic
    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()
        return HttpResponse(headers={"HX-Redirect": self.get_index_url(self.model)})
