from typing import Type

from django.db import models
from django.urls import reverse


def get_index_url(model: Type[models.Model]) -> str:
    return reverse(f"{model._meta.app_label}:index")


def get_add_url(model: Type[models.Model]) -> str:
    return reverse(f"{model._meta.app_label}:add")


def get_edit_url(obj: models.Model) -> str:
    return reverse(f"{obj.__class__._meta.app_label}:edit", kwargs={"pk": obj.pk})


def get_delete_url(obj: models.Model) -> str:
    return reverse(f"{obj.__class__._meta.app_label}:delete", kwargs={"pk": obj.pk})
