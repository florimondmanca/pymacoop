from django.db import models

from dj_fluent import format_value as t


class Customer(models.Model):
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name=t("common-created_at")
    )
    name = models.CharField(max_length=128, verbose_name=t("crm-customers-name"))

    class Meta:
        verbose_name = t("crm-customers-verbose_name")

    def update(self, name):
        self.name = name

    def __str__(self) -> str:
        return self.name


class ProjectManager(models.Manager):
    def with_customer(self):
        return self.select_related("customer")


class Project(models.Model):
    objects = ProjectManager()

    name = models.CharField(max_length=128, verbose_name=t("crm-projects-name"))
    customer = models.ForeignKey(
        "crm.Customer",
        verbose_name=t("crm-projects-customer"),
        on_delete=models.CASCADE,
    )

    class Meta:
        verbose_name = t("crm-projects-verbose_name")

    def update(self, name, customer):
        self.name = name
        self.customer = customer

    def __str__(self) -> str:
        return self.name
