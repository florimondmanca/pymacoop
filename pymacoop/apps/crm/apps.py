from django.apps import AppConfig

from dj_fluent import format_value as t


class CrmConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "pymacoop.apps.crm"
    label = "crm"
    verbose_name = t("crm-title")
