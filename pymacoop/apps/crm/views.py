from django.http import Http404
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse
from djaq import DjaqQuery as DQ

from dj_fluent import format_value as t

from ..tables import paginate
from .forms import CustomerForm, ProjectForm
from .models import Customer, Project
from .tables import CustomerTable, ProjectTable


def list_customers(request):
    customers = list(
        DQ("Customer", "id as pk, name")
        .order_by(request.GET.get("sort", "name"))
        .objs()
    )

    table = CustomerTable(customers)
    table = paginate(table, request)

    return render(
        request,
        "crm/customers/list.html",
        context={
            "table": table,
            "breadcrumb_items": [
                {"title": t("crm-title")},
                {"title": t("crm-customers-title")},
            ],
        },
    )


def add_customer(request):
    if request.method == "POST":
        form = CustomerForm(request.POST)

        if form.is_valid():
            customer = Customer(**form.cleaned_data)
            customer.save()
            return redirect("crm:customers_list")

    return render(
        request,
        "crm/customers/add.html",
        context={
            "form": CustomerForm(),
            "form_action": reverse("crm:customers_add"),
            "breadcrumb_items": [
                {"title": t("crm-title")},
                {
                    "title": t("crm-customers-title"),
                    "path": reverse("crm:customers_list"),
                },
                {"title": t("crm-customers-add-title")},
            ],
        },
    )


def edit_customer(request, pk):
    try:
        customer = next(
            DQ("Customer", "id as pk, name")
            .where("id == {id}")
            .context({"id": pk})
            .dicts()
        )
    except StopIteration:
        raise Http404(pk)

    if request.method == "POST":
        form = CustomerForm(request.POST)

        if form.is_valid():
            obj = Customer.objects.get(pk=pk)
            obj.update(**form.cleaned_data)
            obj.save()

            return redirect("crm:customers_list")

    title = t("crm-customers-edit-title", {"obj": customer["name"]})

    return render(
        request,
        "crm/customers/edit.html",
        context={
            "title": title,
            "form": CustomerForm(initial=customer),
            "breadcrumb_items": [
                {"title": t("crm-title")},
                {
                    "title": t("crm-customers-title"),
                    "path": reverse("crm:customers_list"),
                },
                {"title": title},
            ],
        },
    )


def delete_customer(request, pk):
    customer = get_object_or_404(Customer.objects.only("id"), pk=pk)
    customer.delete()
    return redirect("crm:customers_list")


def list_projects(request):
    projects = list(
        DQ("Project", "id as pk, name, customer.name as customer_name").objs()
    )

    table = ProjectTable(projects)
    table = paginate(table, request)

    return render(
        request,
        "crm/projects/list.html",
        context={
            "table": table,
            "breadcrumb_items": [
                {"title": t("crm-title")},
                {"title": t("crm-projects-title")},
            ],
        },
    )


def add_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)

        if form.is_valid():
            form.save()
            return redirect("crm:projects_list")

    return render(
        request,
        "crm/projects/add.html",
        context={
            "form": ProjectForm(),
            "form_action": reverse("crm:projects_add"),
            "breadcrumb_items": [
                {"title": t("crm-title")},
                {
                    "title": t("crm-projects-title"),
                    "path": reverse("crm:projects_list"),
                },
                {"title": t("crm-projects-add-title")},
            ],
        },
    )


def edit_project(request, pk):
    project = get_object_or_404(Project.objects.with_customer(), pk=pk)

    if request.method == "POST":
        form = ProjectForm(request.POST, instance=project)

        if form.is_valid():
            form.save()
            return redirect("crm:projects_list")

    title = t("crm-projects-edit-title", {"obj": project})

    return render(
        request,
        "crm/projects/edit.html",
        context={
            "title": title,
            "form": ProjectForm(instance=project),
            "breadcrumb_items": [
                {"title": t("crm-title")},
                {
                    "title": t("crm-projects-title"),
                    "path": reverse("crm:projects_list"),
                },
                {"title": title},
            ],
        },
    )


def delete_project(request, pk):
    project = get_object_or_404(Project.objects.only("id"), pk=pk)
    project.delete()
    return redirect("crm:projects_list")
