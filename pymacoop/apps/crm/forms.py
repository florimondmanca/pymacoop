from django import forms

from .models import Customer, Project


class CustomerForm(forms.Form):
    name = forms.CharField(label=Customer.name.field.verbose_name)  # type: ignore


class ProjectForm(forms.ModelForm):
    customer = forms.ModelChoiceField(queryset=Customer.objects.only("id", "name"))

    class Meta:
        model = Project
        fields = ("name", "customer")
