from django.urls import path

from . import views

app_name = "crm"

urlpatterns = [
    path("customers", views.list_customers, name="customers_list"),
    path("customers/add", views.add_customer, name="customers_add"),
    path("customers/edit/<int:pk>", views.edit_customer, name="customers_edit"),
    path("customers/delete/<int:pk>", views.delete_customer, name="customers_delete"),
    path("projects", views.list_projects, name="projects_list"),
    path("projects/add", views.add_project, name="projects_add"),
    path("projects/edit/<int:pk>", views.edit_project, name="projects_edit"),
    path("projects/delete/<int:pk>", views.delete_project, name="projects_delete"),
]
