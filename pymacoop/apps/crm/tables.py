import django_tables2 as tables
from django.urls import reverse

from dj_fluent import format_value as t
from pymacoop.apps.tables.columns import ActionsColumn

from ..tables import DefaultTable
from .models import Customer, Project


class CustomerTable(DefaultTable):
    name = tables.Column(verbose_name=Customer.name.field.verbose_name)  # type: ignore
    actions = ActionsColumn(
        urls={
            "edit": lambda obj: reverse("crm:customers_edit", kwargs={"pk": obj.pk}),
            "delete": lambda obj: reverse(
                "crm:customers_delete", kwargs={"pk": obj.pk}
            ),
        },
        model=Customer,
    )

    class Meta(DefaultTable.Meta):
        pass


class ProjectTable(DefaultTable):
    name = tables.Column(verbose_name=Project.name.field.verbose_name)  # type: ignore
    customer_name = tables.Column(
        verbose_name=t("crm-projects-customer"),
    )
    actions = ActionsColumn(
        urls={
            "edit": lambda obj: reverse("crm:projects_edit", kwargs={"pk": obj.pk}),
            "delete": lambda obj: reverse("crm:projects_delete", kwargs={"pk": obj.pk}),
        },
        model=Project,
    )

    class Meta(DefaultTable.Meta):
        pass
