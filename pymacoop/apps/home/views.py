from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from ...stubs import AuthenticatedHttpRequest


@login_required
def home(request: AuthenticatedHttpRequest):
    return render(request, "home/index.html", {"first_name": request.user.first_name})
