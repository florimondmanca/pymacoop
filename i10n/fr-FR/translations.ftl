common-yes = Oui
common-no = Non
common-created_at = Créé le
common-error = Erreur
common-form-add = Ajouter
common-form-save = Enregistrer
common-form-delete = Supprimer
common-form-delete-confirm = Supprimer cet objet "{$obj}" ?
common-form-delete-confirm-yes = Oui, supprimer
common-form-delete-confirm-no = Non, ne pas supprimer
common-actions = Actions

pagination = Éléments {$start_index}-{$end_index} sur {$total_items}
pagination-title = Pagination
pagination-previous = Page précédente
pagination-next = Page suivante

site-title = Pymacoop

home-title = {$username ->
  [one] Bienvenue, {$username} !
  *[other] Bienvenue !
}

accounts-app = comptes utilisateurs
accounts-login-form-password-change = Vous pouvez modifier votre mot de passe ici.
accounts-login-title = Connexion
accounts-login-next = Veuillez vous connecter pour accéder à cette page
accounts-login-next-authenticated = 
  Vous n'avez pas accès à cette page.
  Pour continuer, veuillez vous connecter avec un compte qui a les accès requis.
accounts-login-form-username = Nom d'utilisateur
accounts-login-form-password = Mot de passe
accounts-login-form-button = Se connecter
accounts-login-form-password-lost = Mot de passe perdu ?
accounts-edit-title = Mon compte
accounts-title = FairRH
accounts-workers-title = Coopérateur-ices et salarié-es
accounts-workers-list-title = Coopérateur-ices et salarié-es
accounts-workers-user-name = Coopérateur-ice ou salarié-e
accounts-workers-joining_date-name = Date d'entrée
accounts-workers-leaving_date-name = Date de sortie
accounts-workers-annual_salary-name = Salaire brut annuel
accounts-workers-transport_fee-name = Abonnement transports
accounts-workers-sustainable_mobility_fee-name = Forfait mobilités durables
accounts-workers-executive_position-name = Statut cadre
accounts-workers-health_insurance-name = Mutuelle
accounts-workers-contract-name = Contrat
accounts-workers-contract-cdi = CDI
accounts-workers-contract-cdd = CDD
accounts-workers-working_time-name = Temps de travail
accounts-workers-working_time-full_time = Temps plein
accounts-workers-working_time-part_time = Temps partiel
accounts-workers-add-title = Ajouter un-e coopérateur-ice ou salarié-e
accounts-workers-edit-title = Modifier {$worker}

header-account = Mon compte
header-logout = Se déconnecter

dashboard-title = Dashboard

faircalendar-title = FairCalendar
faircalendar-event-verbose_name = Événement
faircalendar-event-type-name = Type
faircalendar-event-type-mission = Mission
faircalendar-event-type-dojo = Dojo
faircalendar-event-type-support = Supports
faircalendar-event-type-other = Autre
faircalendar-event-type-formation_conference = Formation / Conférence
faircalendar-event-type-Autre = Support
faircalendar-event-date-name = Date
faircalendar-event-project-name = Projet
faircalendar-event-user-name = Coopérateur-ice / salarié-e
faircalendar-event-comment-name = Commentaire
faircalendar-event-add-title = CRA du {$date}
faircalendar-event-edit-title = Édition du CRA du {$date}

crm-title = FairCRM

crm-customers-title = Clients
crm-customers-add-title = Ajouter un client
crm-customers-edit-title = Édition du client "{$obj}"
crm-customers-verbose_name = Client
crm-customers-name = Nom

crm-projects-title = Projets
crm-projects-add-title = Ajouter un projet
crm-projects-edit-title = Édition du projet "{$obj}"
crm-projects-app = Projets
crm-projects-verbose_name = Projet
crm-projects-name = Nom
crm-projects-customer = Client
