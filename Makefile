venv = venv
bin = ${venv}/bin/
apps = pymacoop/apps
src = pymacoop/ dj_fluent/

all: help

help: ## Display this message
	@grep -E '(^[a-zA-Z0-9_\-\.]+:.*?##.*$$)|(^##)' Makefile | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m## /[33m/'

install:
	python3 -m venv ${venv}
	${bin}pip install -U pip setuptools wheel
	${bin}pip install -r requirements.txt
	make initdb
	make assets

manage:
	${bin}python manage.py ${CMD}

assets: ## Build assets
	make manage CMD="collectstatic --noinput"

initdb: ## Setup database
	make migrate
	make initdata

initdata: ## Load initial dev data into database (may fail if database is non-empty)
	make manage CMD="loaddata dev"

start:
	docker-compose up -d
	make manage CMD="runserver localhost:8001"

stop:
	docker-compose stop

shell: #- Run Django shell
	make manage CMD=shell

dbshell: #- Connect to database shell
	make manage CMD=dbshell

cleardb-forcefully: #- Clear database contents forcefully
	make manage CMD=sqlflush | make manage CMD=dbshell

app: #- Create a new app
	mkdir -p ${apps}/$(name)
	make manage CMD="startapp $(name) ${apps}/$(name)"

migrations: #- Generate migrations for all apps
	make manage CMD=makemigrations

appmigrations: #- Generate migrations for a single app
	make manage CMD="makemigrations $(app)"

migrate: #- Run migrations
	make manage CMD=migrate

format: #- Run code formatting
	${bin}black ${src}
	${bin}ruff check --fix ${src}

check: #- Run code checks and linters
	${bin}black --check ${src}
	${bin}ruff check ${src}
	${bin}mypy ${src}
	make manage CMD=check

test: #- Run tests
	${bin}pytest $(args)
