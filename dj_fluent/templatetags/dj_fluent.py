from django import template
from django.conf import settings
from django.template.base import kwarg_re

from .._translations import format_value

register = template.Library()


@register.filter(name="translate")
def do_filter_translate(msg_id: str) -> str:
    locales = getattr(settings, "FLUENT_LOCALES", ["en-US"])
    return format_value(msg_id, locales=locales)


@register.tag(name="translate")
def do_tag_translate(parser, token):
    bits = token.split_contents()

    if len(bits) <= 1:
        raise template.TemplateSyntaxError(
            f"{bits[0]!r} requires at least one argument, the message ID."
        )

    tag_name = bits[0]
    msg_id = template.Variable(bits[1])
    kwargs = {}
    error_msg = f"Arguments to {tag_name!r} tag must be key=value pairs"

    for bit in bits[2:]:
        match = kwarg_re.match(bit)
        if not match:
            raise template.TemplateSyntaxError(error_msg)
        name, value = match.groups()
        if name:
            kwargs[name] = template.Variable(value)
        else:
            raise template.TemplateSyntaxError(error_msg)

    return TranslateNode(msg_id, kwargs)


class TranslateNode(template.Node):
    def __init__(self, msg_id, kwargs):
        self._msg_id = msg_id
        self._kwargs = kwargs

    def render(self, context: template.Context) -> str:
        msg_id = self._msg_id.resolve(context)
        kwargs = {}

        for name, value in self._kwargs.items():
            try:
                value = value.resolve(context)
            except template.VariableDoesNotExist:
                value = ""
            kwargs[name] = value

        locales = getattr(settings, "FLUENT_LOCALES", ["en-US"])

        return format_value(msg_id, kwargs, locales=locales)
