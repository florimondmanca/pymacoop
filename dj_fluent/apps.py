from django.apps import AppConfig


class DjFluentConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "dj_fluent"
