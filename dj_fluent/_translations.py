from django.conf import settings
from fluent.runtime import FluentLocalization, FluentResourceLoader

MISSING_TRANSLATIONS = set()


def _get_l10n(locales, loader=None, resource_ids=None):
    if loader is None:
        roots = getattr(settings, "FLUENT_ROOTS", "l10n/{locale}")
        loader = FluentResourceLoader(roots)

    if resource_ids is None:
        resource_ids = getattr(settings, "FLUENT_RESOURCE_IDS", ["main.ftl"])

    return FluentLocalization(locales, resource_ids, loader)


def format_value(
    msg_id: str,
    kwargs: dict | None = None,
    *,
    locales: list[str] | None = None,
) -> str:
    if locales is None:
        locales = getattr(settings, "FLUENT_LOCALES", ["en-US"])

    kwargs = (
        None if kwargs is None else {key: str(value) for key, value in kwargs.items()}
    )

    value = _get_l10n(locales).format_value(msg_id, kwargs)

    if value == msg_id:
        MISSING_TRANSLATIONS.add(msg_id)

    return value
