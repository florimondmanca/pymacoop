from django.core.checks import Warning, register

from ._translations import MISSING_TRANSLATIONS


@register()
def check_missing_translations(app_configs, **kwargs):
    errors = []

    if MISSING_TRANSLATIONS:
        msg = "Some translations are missing: \n%s" % "\n".join(
            sorted(MISSING_TRANSLATIONS)
        )
        errors.append(
            Warning(
                msg,
                hint="Edit the translations file to resolve.",
                obj="dj_fluent",
                id="dj_fluent.E001",
            )
        )

    return errors
